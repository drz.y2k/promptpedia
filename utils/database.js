import mongoose from 'mongoose';

let isConnected = false;

//este metodo define la función que se encarga de conectar nuestro proyecto a nuestra bd de mongo
export const connectToDB = async () => {
	// esta configuración nos permite hacer busquedas mas correctas
	//ya que nos obliga a hacer busquedas unicamente con los campos que definimos en el schema
	mongoose.set('strictQuery', true);

	if (isConnected) {
		//si ya está conectado imprimimos en consola y retornamos
		console.log('mongodb is already connected');
		return;
	}

	//sino está conectado entonces nos conectamos con nuestra URI de mongo atlas
	try {
		await mongoose.connect(process.env.MONGODB_URI, {
			dbName: "share_prompt",
			useNewUrlParser: true,
			useUnifiedTopology: true,
		})

		//si la conexión es exitosa, entonces cambiamos el valor de isConnected a true
		//para que si se llama el método de nuevo no se vuelva a conectar
		isConnected = true;
		console.log('mongodb is connected');


	} catch (e) {
		console.log(e);
	}

}