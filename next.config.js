// esta configuración estaba predefinida en un gist de github del ejemplo en cuestión
/** @type {import('next').NextConfig} */
const nextConfig = {
	experimental: {
	  serverComponentsExternalPackages: ["mongoose"],
	},
	images: {
	  domains: ['lh3.googleusercontent.com'],
	},
	webpack(config) {
	  config.experiments = {
		...config.experiments,
		topLevelAwait: true,
	  }
	  return config
	}
  }
  
  module.exports = nextConfig