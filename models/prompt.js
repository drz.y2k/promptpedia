import { Schema, model, models } from 'mongoose';

//creamos un schema de mongoose, para validar la estructura que tendrá nuestro
//documento en la base de datos
const PromptSchema = new Schema({
	//creamos el atributo creator, que será un id de un usuario que tengamos
	//en la base de datos, y nos ayudará a saber quién creó el prompt,
	//esto por decirlo de algún modo nos ayuda a crear una referencia entre
	//prompts y usuarios
	creator: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	//este es el prompt que es el texto que guardamos en la base de datos
	//y esta validado para que sea requerido
	prompt:{
		type: String,
		required: [true, 'Prompt is required']
	},
	//este es el tag que es la etiqueta que guardamos de nuestro
	//prompt y nos ayuda a clasificarlo
	tag:{
		type: String,
		required: [true, 'Tag is required']
	},
});

//creamos nuestra instancia de prompt si no existe, si existe usamos la que 
//ya fue creada y la exportamos
const Prompt = models.Prompt || model('Prompt', PromptSchema);

export default Prompt;