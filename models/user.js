import { Schema, model, models } from 'mongoose';

//creamos este modelo y schema de mongoose, para validar la estructura que tendrá nuestro
//documento en la base de datos

//tenemos valores que son tuplas, como unique, required, match, etc
//el primer elemento de esa tupla es el valor, y el segundo es el mensaje de error que se mostrará
const userSchema = new Schema({
	email: {
		type: String,
		unique: [true, 'Email already exists'],
		required: [true, 'Email is required'],
	},
	username: {
		type: String,
		required: [true, 'Username is required'],
		match: [/^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/, 'Username is invalid, it should contain 8-20 alphanumeric characters and be unique'],

	},
	image: {
		type: String
	}
});

//si ya existe un modelo con el nombre User, entonces lo usamos, si no, lo creamos
//esta validación es necesaria en next, porque si no, se crea un nuevo modelo cada vez que se
//hace un request a la base de datos, ya que next funciona con serverless functions
const User = models.User || model('User', userSchema);

export default User;