'use client';

import React from 'react';
import { SessionProvider } from 'next-auth/react';

//usamos este componente para envolver a nuestra aplicación
//para que nuestra aplicación tenga acceso a la sesión de next-auth
const Provider = ({ children, session }) => {
	return <SessionProvider session={session}>{children}</SessionProvider>;
};

export default Provider;
