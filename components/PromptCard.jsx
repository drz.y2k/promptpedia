'use client';

import { useState } from 'react';
import Image from 'next/image';
import { useSession } from 'next-auth/react';
import { usePathname, useRouter } from 'next/navigation';

import React from 'react';

//este es el componente card, y recibe cada uno de los posts
const PromptCard = ({ post, handleTagClick, handleEdit, handleDelete }) => {
	const { data: session } = useSession();
	const pathName = usePathname();
	const router = useRouter();

	//con este estado vemos si el elemento se ha copiado al portapapeles
	//esto lo usamos para cambiar el icono de copiar a un check

	const [copied, setCopied] = useState('');

	const handleProfileClick = () => {	
		if (post.creator._id === session?.user.id) return router.push("/profile");
	
		router.push(`/profile/${post.creator._id}?name=${post.creator.username}`);
	  };

	//con esta funcion cuando se copia un elemento al portapapeles,
	//cambiamos el icono, y luego de 3 segundos lo regresamos a su estado original
	const handleCopy = () => {
		setCopied(post.prompt);
		navigator.clipboard.writeText(post.prompt);
		setTimeout(() => setCopied(''), 3000);
	};

	return (
		<div className="prompt_card">
			<div className="flex justify-between items-start gap-5">
				<div
					className="flex-1 flex justify-start items-center gap-3 cursor-pointer"
					onClick={handleProfileClick}
				>
					{/* agregamos la imagen del usuario usando el componente image que es una versión
					optimizada de la etiqueta img */}
					<Image
						src={post.creator.image}
						alt="user_image"
						width={40}
						height={40}
						className="rounded-full object-contain"
					/>
					{/* ponemos nombre de usuario y email del creador del prompt */}
					<div className="flex flex-col">
						<h3 className="font-satoshi font-semibold text-gray-900">
							{post.creator.username}
						</h3>
						<p className="font-inter text-sm text-gray-500">
							{post.creator.email}
						</p>
					</div>
				</div>

				{/* agregamos el boton de copiar */}
				<div className="copy_btn" onClick={handleCopy}>
					<Image
						src={
							//condicionalmente cambiamos el icono dependiendo de si el prompt se ha copiado
							copied === post.prompt
								? '/assets/icons/tick.svg'
								: '/assets/icons/copy.svg'
						}
						width={12}
						height={12}
					/>
				</div>
			</div>

			{/* mostramos el contenido del prompt */}
			<p className="my-4 font-satoshi text-sm text-gray-700">
				{post.prompt}
			</p>
			<p
				className="font-inter text-sm blue_gradient cursor-pointer"
				onClick={() => {
					//TODO
					handleTagClick && handleTagClick(post.tag);
				}}
			>
				#{post.tag}
			</p>

			{/* vamos a mostrar botones de editar y eliminar debajo de cada publicación
			pero estos se van a mostrar solo que tengamos un id de usuario, y que ese id de usuario sea igual al 
			id del creador del post, ademas de que el pathname sea el de /profile, es decir
			que nos encontremos en nuestro perfil viendo nuestros prompts, de otro modo no se mostrará */}
			{session?.user?.id === post.creator._id &&
				pathName === '/profile' && (
					<div className="mt-5 flex-center gap-4 border-t border-gray-100 pt-3">
						<p
							className="font-inter text-sm green_gradient cursor-pointer"
							onClick={handleEdit}
						>
							Edit
						</p>
						<p
							className="font-inter text-sm orange_gradient cursor-pointer"
							onClick={handleDelete}
						>
							Delete
						</p>
					</div>
				)}
		</div>
	);
};

export default PromptCard;
