// este será un elemento del lado del cliente
'use client';

//importamos algunos componentes de next, como el link y la imagen
//qe son componentes ya optimizados para su uso con next
import Link from 'next/link';
import Image from 'next/image';
//vamos a importar utilidades de next para la autenticación
import { signIn, signOut, useSession, getProviders } from 'next-auth/react';
//vamos a usar hooks entonces los importamos, y por eso
//este componente debe ser del lado del cliente
import { useState, useEffect } from 'react';

const Nav = () => {
	// const isUserLoggedIn = false;
	//extraemos los datos de session de nuestro hook de next-auth
	//llamado use session, dede ahípodemos obtener los datos de nuestro usuario autenticado
	//como son el nombre, la imagen, el email, etc, en este caso usamos el provider de Google,
	//y usaremos este mismo valor de session para renderizar condicionalmente los botones de autenticación
	const { data: session } = useSession();

	const [providers, setProviders] = useState(null);
	const [toggleDropdown, setToggleDropdown] = useState(false);

	// obtenemos los providers de autenticación de next-auth y los asignamos a nuestro estado local
	useEffect(() => {
		const assignProviders = async () => {
			const response = await getProviders();

			setProviders(response);
		};

		assignProviders();
	}, []);

	return (
		<nav className="flex-between w-full mb-16 pt-3 px-4 z-100">
			{/* este link retorna a la ruta principal por eso ponemos eso en el href */}
			<Link href="/" className="flex gap-2 flex-center z-10">
				<Image
					src="/assets/images/logo.svg"
					alt="Promptopia logo"
					width={30}
					height={30}
					className="object-contain"
				/>
				<p className="logo_text">Promptopia</p>
			</Link>


			{/* desktop navigation */}
			<div className="sm:flex hidden">
				{session?.user ? (
					<div className="flex gap-3 md:gap-5">
						<Link href="/create-prompt" className="black_btn">
							Create Post
						</Link>
						<button
							type="button"
							onClick={signOut}
							className="outline_btn"
						>
							Sign Out
						</button>

						<Link href={'/profile'}>
							<Image
								src={session?.user.image}
								width={37}
								height={37}
								className="rounded-full"
								alt="profile"
							></Image>
						</Link>
					</div>
				) : (
					//si el usuario no está autenticado vamos a mostrar los botones de autenticación
					//de los providers que tenemos disponibles gracias a next-auth
					<>
						{providers &&
							Object.values(providers).map((provider) => {
								return <button
									type="button"
									key={provider.name}
									onClick={async() => await signIn(provider.id)}
									className="black_btn"
								>
									Sign in
								</button>;
							})}
					</>
				)}
			</div>
			{/* mobile navigation */}
			{/* este es el mismo código que el de arriba pero es para dispositivos móviles, igual 
			agregamos nuestra imáagen de nuestro avatar, donde además tenemos un dropdown, para poner las opciones
			de acceder a nuestro perfil, crear un prompt, o cerrar sesión
			
			Si estamos en dispositivos móviles y no tenemos sesión iniciada también mostraremos nuestros providers de
			autenticación obtenidos de next-auth*/}
			<div className="sm:hidden flex relative">
				{session?.user ? (
					<div className="flex">
						<Image
							src={session?.user.image}
							width={37}
							height={37}
							className="rounded-full"
							alt="profile"
							onClick={() => {
								setToggleDropdown((prev) => !prev);
							}}
						/>
						{toggleDropdown && (
							<div className="dropdown z-[100]">
								<Link
									href={'/profile'}
									className="dropdown_link"
									onClick={() => setToggleDropdown(false)}
								>
									My Profile
								</Link>
								<Link
									href={'/create-prompt'}
									className="dropdown_link"
									onClick={() => setToggleDropdown(false)}
								>
									Create Prompt
								</Link>
								<button
									type="button"
									onClick={() => {
										setToggleDropdown(false);
										signOut();
									}}
									className="mt-5 w-full black_btn"
								>
									Sign Out
								</button>
							</div>
						)}
					</div>
				) : (
					<>
						{providers &&
							Object.values(providers).map((provider) => {
								<button
									type="button"
									key={provider.name}
									onClick={async() => await signIn(provider.id)}
									className="black_btn"
								>
									Sign in
								</button>;
							})}
					</>
				)}
			</div>
		</nav>
	);
};

export default Nav;
