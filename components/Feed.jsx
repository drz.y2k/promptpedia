'use client';
//usamos hooks entonces será un componente del lado del cliente
import { useState, useEffect } from 'react';
//importamos un componente que usaremos
import PromptCard from './PromptCard';

//este componente solo se usará en este archivo, por eso no lo exportamos
//y lo usamos solo aquí, este componente define el layout de los prompts que obtenemos
//y usamos un map para imprimir todos esos prompts
const PromptCardList = ({ data, handleTagClick }) => {
	return (
		<div className="mt-16 prompt_layout">
			{data.map((post) => (
				<PromptCard
					key={post._id}
					post={post}
					handleTagClick={handleTagClick}
				/>
			))}
		</div>
	);
};

const Feed = () => {
	const [allPosts, setAllPosts] = useState([]);
	//creamos nuestros estados para almacenar el texto de busqueda, y el arreglo
	//de posts que obtenemos de la base de datos
	//Search states
	const [searchText, setSearchText] = useState('');
	const [searchTimeout, setSearchTimeout] = useState(null);
	const [searchedResults, setSearchedResults] = useState([]);

	const fetchPosts = async () => {
		const res = await fetch('/api/prompt');
		const data = await res.json();
		setAllPosts(data);
	};

	//creamos un useEffect para que obtenga los posts apenas cargue el componente
	useEffect(() => {
		fetchPosts();
	}, []);

	const filterPrompts = (searchtext) => {
		const regex = new RegExp(searchtext, 'i'); // 'i' flag for case-insensitive search
		return allPosts.filter(
			(item) =>
				regex.test(item.creator.username) ||
				regex.test(item.tag) ||
				regex.test(item.prompt)
		);
	};

	const handleSearchChange = (e) => {
		clearTimeout(searchTimeout);
		setSearchText(e.target.value);

		// debounce method
		setSearchTimeout(
			setTimeout(() => {
				const searchResult = filterPrompts(e.target.value);
				setSearchedResults(searchResult);
			}, 500)
		);
	};

	const handleTagClick = (tagName) => {
		setSearchText(tagName);

		const searchResult = filterPrompts(tagName);
		setSearchedResults(searchResult);
	};

	return (
		<section className="feed">
			<form className="relative w-full flex-center">
				{/* creamos un input que nos ayudará a buscar por tag o username
				dentro de nuestro listado de prompts */}
				<input
					type="text"
					placeholder="Search for a tag or a username"
					value={searchText}
					onChange={handleSearchChange}
					required
					className="search_input peer"
				/>
			</form>
			{/* debajo de este cuadro de busqueda, imprimimos nuestro layout que contiene
			nuestra lista de prompts, esta interfaz la estamos creando en este mismo archivo */}

			{searchText ? (
				<PromptCardList
					data={searchedResults}
					handleTagClick={handleTagClick}
				/>
			) : (
				<PromptCardList
					data={allPosts}
					handleTagClick={handleTagClick}
				/>
			)}
		</section>
	);
};

export default Feed;
