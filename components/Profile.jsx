import PromptCard from './PromptCard';

//creamos este componente perfil, donde vamos a tener el nombre del dueño del perfil
//una descripcion y los posts asociados a este perfil
const Profile = ({ name, desc, data, handleEdit, handleDelete }) => {
	return (
		<section className="w-full">
			<h1 className="head_text text-left">
				<span className="blue_gradient">{name} Profile</span>
			</h1>
			<p className="desc text-left">{desc}</p>

			<div className="mt-10 prompt_layout">
				{data.map((post) => (
					<PromptCard
						key={post._id}
						post={post}
						handleEdit={() => handleEdit && handleEdit(post)}
						handleDelete={() => handleDelete && handleDelete(post)}
					/>
				))}
			</div>
		</section>
	);
};

export default Profile;
