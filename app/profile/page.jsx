'use client';

import { useState, useEffect } from 'react';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/navigation';

import Profile from '@components/Profile';

const MyProfile = () => {
	//creamos un router para poder redireccionar a otras paginas
	const router = useRouter();

	//obtenemos la informacion de sesion
	const { data: session } = useSession();
	//creamos un estado para almacenar los posts que obtenemos del back
	const [posts, setPosts] = useState([]);

	useEffect(() => {
		const fetchPosts = async () => {
			//obtenemos los posts de nuestro backend enviando el id del usuario
			const res = await fetch(`/api/users/${session?.user?.id}/posts`);
			const data = await res.json();
			//asignamos nuestros posts al estado
			setPosts(data);
		};

		//llamamos esta funcion si contamos con una sesion activa
		if (session?.user?.id) fetchPosts();
		// fetchPosts();
	}, []);

	const handleEdit = (post) => {
		//enviamos a nuestro usuario a la pagina de edicion de posts para que pueda editar más fácil
		//aqui enviamos en la ruta el id del post a editar
		router.push(`/update-prompt?id=${post._id}`);
	};

	//creamos la funcion de eliminar, donde se pide confirmación usando un confirm, y en caso
	//de ser positiva se procede con la eliminacion del post, despues, se filtra el post eliminado
	//de la lista de posts y se actualiza el estado ya sin ese post
	const handleDelete = async (post) => {
		const hasConfirmed = confirm(
			'Are you sure you want to delete this prompt?'
		);

		if (hasConfirmed) {
			try {
				await fetch(`/api/prompt/${post._id.toString()}`, {
					method: 'DELETE',
				});

				const filteredPost=posts.filter(p=>p._id!==post._id);

				setPosts(filteredPost);

			} catch (err) {
				console.log(err);
			}
		}
	};

	//creamos el componente perfil y le pasamos los datos de los posts de nuestros
	//promps asociados a nuestro perfil
	return (
		<Profile
			name="My"
			desc="Welcome to your personalized profile page"
			data={posts}
			handleEdit={handleEdit}
			handleDelete={handleDelete}
		/>
	);
};

export default MyProfile;
