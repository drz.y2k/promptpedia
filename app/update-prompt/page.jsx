'use client';

//este archivo es practicamente una copia del archivo de create prompt solo se cambian algunas cosas

import { useEffect, useState } from 'react';
import { useRouter, useSearchParams } from 'next/navigation';

import Form from '@components/Form';

const EditPrompt = () => {
	const router = useRouter();

	// const { data: session } = useSession();

	//este bloque obtiene el id de la ruta para poder usarlo en este lado, en el front
	const searchParams = useSearchParams();
	const promptId = searchParams.get('id');

	const [submitting, setSubmitting] = useState(false);

	const [post, setPost] = useState({
		prompt: '',
		tag: '',
	});

	useEffect(() => {
		//obtenemos los datos del post a editar
		//el id a consultar se obtiene de la ruta y se hace la peticion para obtener
		//los datos de ese post
		const getPromptDetails = async () => {
			const res = await fetch(`/api/prompt/${promptId}`);
			const data = await res.json();
			setPost({
				prompt: data.prompt,
				tag: data.tag,
			});
		};

		if (promptId) getPromptDetails();
		//esto se ejecutará cada que el id cambie, recordar que el id viene de la ruta
	}, [promptId]);

	const updatePrompt = async (e) => {
		e.preventDefault();
		setSubmitting(true);

		//si no tenemos el id del post a editar, no se puede editar 
		//entonces retornamos un alert
		if (!promptId) return alert('Prompt ID not found');

		//caso contrario usamos el patch que definimos en el backend
		//y enviamos el body con los datos  editar
		try {
			const res = await fetch(`/api/prompt/${promptId}`, {
				method: 'PATCH',
				body: JSON.stringify({
					prompt: post.prompt,
					tag: post.tag,
				}),
			});
			//si todo sale bien redirigimos al usuario a la pagina principal
			if (res.ok) {
				router.push('/');
			}
		} catch (err) {
			console.log(err);
		} finally {
			setSubmitting(false);
		}
	};

	return (
		<Form
			type="Edit"
			post={post}
			setPost={setPost}
			submitting={submitting}
			handleSubmit={updatePrompt}
		/>
	);
};

export default EditPrompt;
