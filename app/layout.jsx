// importamos nuestro css global de la carpeta styles
import '@styles/global.css';

import Nav from '@components/Nav';
import Provider from '@components/Provider';

export const metadata = {
	title: 'Promptopia',
	description: 'Discover & Share AI Prompts',
};

const RootLayout = ({ children }) => {
	return (
		// en nuestro root layout regresamos nuestro html y body tags
		<html lang="en">
			<body>
				{/* Envolvemos toda nuestra aplicación en el provider que provee la sesión de next-auth */}
				<Provider>
					{/* creamos 2 divs, 1 para el gradiente que estará de fondo y otro para contener nuestra app y renderizar
				el contenido de nuestras rutas */}
					<div className="main -z-10">
						<div className="gradient" />
					</div>
					<Nav />
					<main className="app">{children}</main>
				</Provider>
			</body>
		</html>
	);
};

export default RootLayout;
