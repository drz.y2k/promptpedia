'use client';

//usamos nuestro useState entonces debemos hacer el componente de tipo cliente
import { useState } from 'react';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/navigation';

//vamos a importar y usar nuestro componente Form
import Form from '@components/Form';

//creamos nuestro componente CreatePrompt
const CreatePrompt = () => {
	const router = useRouter();
	//vamos a usar nuestro enrutador, y vamos tambien a obtener
	//la data de la sesión activa
	const { data: session } = useSession();

	//creamos nuestro estado de submitting, esto nos ayudará a saber cuando
	//se está enviando información al backend, y así poder mostrar un mensaje
	const [submitting, setSubmitting] = useState(false);
	//creamos nuestro estado de post, que va a ser un objeto con dos propiedades
	//prompt y tag, y vamos a inicializarlo con un string vacío, esto es lo que enviamos al backend
	//para crear un nuevo prompt
	const [post, setPost] = useState({
		prompt: '',
		tag: '',
	});

	//creamos nuestra función que crea el prompt, recibe un evento
	//y lo que hace es prevenir el comportamiento por defecto del formulario, que es
	//recargar la página, y luego seteamos el estado de submitting a true
	//mientras se envía la información al backend
	const createPrompt = async (e) => {
		e.preventDefault();
		setSubmitting(true);

		try {
			//usamos await para enviar la información al backend, y luego
			//usamos router.push para redirigir al usuario a la página principal
			//si todo sale bien
			const res = await fetch('/api/prompt/new', {
				method: 'POST',
				//enviamos como body un objeto con las propiedades prompt, userId y tag
				//estas se asignan desde el componente hijo form, y se pasan al padre que es este
				//archivo por medio del setPost que es una funcion que  enviamos como prop
				body: JSON.stringify({
					prompt: post.prompt,
					userId: session?.user.id,
					tag: post.tag,
				}),
			});

			if (res.ok) {
				router.push('/');
			}
		} catch (err) {
			console.log(err);
		} finally {
			//finalmente pase lo que pase seteamos el estado de submitting a false
			setSubmitting(false);
		}
	};

	return (
		<Form
			type="Create"
			post={post}
			setPost={setPost}
			submitting={submitting}
			//enviamos la funcion que crea el prompt como prop
			//para poder usarla desde el componente hijo
			handleSubmit={createPrompt}
		/>
	);
};

export default CreatePrompt;
