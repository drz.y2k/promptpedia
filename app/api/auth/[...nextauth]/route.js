import NextAuth from "next-auth";
import GoogleProvider from "next-auth/providers/google";

//esta ruta funciona como un middleware, se coloca en la carpeta que indica la documentación
//y se importa nexthauth y el provider que se usará que en este caso es el de google

//vamos a importar el modelo y la conexión a la bd porque desde aquí crearemos al usuario
//en caso de que no exista
import User from "@models/user";
import { connectToDB } from "@utils/database";

const handler = NextAuth({
	// definimos nuestros providers, en este caso el de google
	//con las variables de entorno que generamos en nuestra consola de google
	providers: [
		GoogleProvider({
			clientId: process.env.GOOGLE_ID,
			clientSecret: process.env.GOOGLE_SECRET
		})
	],
	//definomos los callbacks, que son funciones que se ejecutan cuando se inicia sesión,
	//en este caso cuando inicia sesión se crea el usuario en la bd si no existe, y si existe
	//retornamos true, tambien usamos el callback session para validar la sesión buscando el usuario que coincida
	//en email con el que acaba de ingresar, y asignamos el id de ese usuario, a nuestro objeto session.user
	callbacks: {
		async session({ session }) {
			const sessionUser = await User.findOne({ email: session.user.email });

			session.user.id = sessionUser._id.toString();
			

			return session;

		},
		async signIn({ profile }) {
			try {
				await connectToDB();

				//check if user already exists
				const userExists = await User.findOne({ email: profile.email });

				//if not, create user
				if (!userExists) {
					await User.create({
						email: profile.email,
						username: profile.name.replace(" ", "").replace(".","").toLowerCase(),
						image: profile.picture
					})
				}


				return true;
			} catch (e) {
				console.log(e);
				return false;
			}

		}
	}

});

export { handler as GET, handler as POST };