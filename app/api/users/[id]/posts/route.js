import { connectToDB } from "@utils/database";
import Prompt from "@models/prompt";

//params se obtiene automaticamente de la ruta, en este caso, el id del usuario
//que viene del folder llamado [id] que se convierte a un parámetro dinamico
export const GET = async (req, { params }) => {
	try {
		await connectToDB();

		//filtramos los prompts por el id del usuario para que solo se muestren los prompts
		//donde el id del creador coincida con el id enviado en la ruta
		const prompts = await Prompt.find({ creator: params.id }).populate('creator');

		return new Response(JSON.stringify(prompts), {
			status: 200
		})
	} catch (err) {
		return new Response('Failed to fetch all prompts', {
			status: 500
		})
	}
}