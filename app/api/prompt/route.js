import { connectToDB } from '@utils/database.js';
import Prompt from '@models/prompt.js';

//creamos este metodo get para obtener todos los prompts del usuario
//que está logueado
export const GET = async (req) => {
	try {
		await connectToDB();
		//la funcion find nos retorna todos los prompts sin un filtro en especifico
		//y usamos populate para que los datos del objeto creator tengan los datos
		//de la referencia conectada, en este caso, del usuario creador del post,
		//recordar que en nuestro modelo estamos haciendo esta referencia, aquí solo
		//la estamos usando
		const prompts = await Prompt.find({}).populate('creator');

		//retornamos los prompts que encontramos en formato json
		//y retornamos un status code de 200 que significa que todo salió bien
		return new Response(JSON.stringify(prompts), {
			status: 200
		})
	} catch (err) {
		//si algo falla retornamos error 500 y un mensaje descriptivo
		return new Response('Failed to fetch all prompts', {
			status: 500
		})
	}
}