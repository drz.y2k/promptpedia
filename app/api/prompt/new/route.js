import { connectToDB } from '@utils/database.js';
// importamos nuestro modelo de prompts para usarlo en la inserción
import Prompt from '@models/prompt.js';

//creamos nuestro método post el cual nos servira para agregar
//promps a nuestroa bd
export const POST = async (req) => {
	//obtenemos el userId,el prompt y el tag del request
	const { userId, prompt, tag } = await req.json();

	try {
		//en cada request a nuestro back creamos nuestra conexión
		//esto es porque esto es una lambda function, cada que se ejecuta, hace su 
		//trabajo y se destruye, por lo que no podemos tener una conexión abierta
		await connectToDB();
		//creamos el objeto usando el modelo
		const newPrompt = new Prompt({
			creator: userId,
			prompt,
			tag
		});
		//lo guardamos en nuestra bd de mongo
		await newPrompt.save();
		//retornamos el status code 201 que significa que se creó correctamente
		//y retornamos el objeto que creamos
		return new Response(JSON.stringify(newPrompt), {
			status: 201
		})

	} catch (err) {
		//en caso de error retornamos un status code 500 que significa error
		//y retornamos un string que indique este error
		return new Response('Failed to create a new prompt', {
			status: 500
		})
	}
}