import { connectToDB } from '@utils/database.js';
import Prompt from '@models/prompt.js';

//creamos los metodos para obtener, actualizar y eliminar prompts
export const GET = async (req, { params }) => {
	try {
		await connectToDB();
		//obtenemos el prompt en base al id que enviamos en la ruta
		const prompt = await Prompt.findById(params.id).populate('creator');

		//sino existe retornamos que no lo encontramos
		if (!prompt) {
			return new Response('Prompt not found', {
				status: 404
			})
		}

		//si existe retornamos el prompt
		return new Response(JSON.stringify(prompt), {
			status: 200
		})
	} catch (err) {
		return new Response('Failed to fetch prompt', {
			status: 500
		})
	}
}

//usamos el patch para editar nuestro prompt, recibimos el param de la ruta y de req extraemos
//el prompt y el tag para agregarlos al prompt que queremos editar
export const PATCH = async (req, { params }) => {
	const { prompt, tag } = await req.json();


	try {
		await connectToDB();

		//obtenemos el prompt que queremos editar
		const existingPrompt = await Prompt.findById(params.id);

		if (!existingPrompt) {
			return new Response('Prompt not found', {
				status: 404
			})
		}

		//modificamos el prompt
		existingPrompt.prompt = prompt;
		existingPrompt.tag = tag;

		//intentamos guardar el prompt
		await existingPrompt.save();

		//retornamos el prompt modificado
		return new Response(JSON.stringify(existingPrompt), {
			status: 200
		})

	} catch (err) {
		//si algo falla retornamos el error
		return new Response('Failed to update prompt', {
			status: 500
		})
	}

}

//usamos el delete para eliminar un prompt, recibimos el param de la ruta
export const DELETE = async (req, { params }) => {
	try{
		await connectToDB();

		//simplemente encontramos el prompt y lo eliminamos con 
		//este metodo findByIdAndDelete
		await Prompt.findByIdAndDelete(params.id);

		//si todo sale bien retornamos un mensaje de exito
		return new Response("Prompt deleted successfully", {
			status: 200
		})

	}catch(err){
		//si algo falla retornamos el error
		return new Response('Failed to delete prompt', {
			status: 500
		})
	}
}